// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App';
import router from './router';
import TeclaBlanca from './components/TeclaBlanca';
import TeclaNegra from './components/TeclaNegra';
import SocketService from './services/client';
import './styles/app.scss';

Vue.use(BootstrapVue);
Vue.config.productionTip = false;

Vue.component('tecla-blanca', TeclaBlanca);
Vue.component('tecla-negra', TeclaNegra);

const myMixin = {
  data() {
    return {
      url: '192.168.32.104:3001',
      socketService: Object,
    };
  },
  created() {
    this.socketService = new SocketService(this.url);
  },
  methods: {
    getSocket() {
      return this.socketService;
    },
  },
};

// define a component that uses this mixin
Vue.mixin(myMixin);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
