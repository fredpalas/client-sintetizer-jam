/* eslint-disable */
import io from "socket.io-client";

export default class SocketService {

    socket;

    constructor(url) {
        this.socket = io(url);
    }

    sendNote(message) {
        this.socket.emit("new note", message);
    }
}